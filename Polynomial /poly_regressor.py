#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 21 18:19:40 2017

@author: wmk
"""

#Polynomial

import numpy as np
import matplotlib.pyplot as pyplot
import pandas as pd

#Import dataset
dataset = pd.read_csv('Position_Salaries.csv')
X = dataset.iloc[:,1].values
y = dataset.iloc[:, 2].values