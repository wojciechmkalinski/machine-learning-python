#Linear regression
#Using calculus of probabilities as a math patterns. 
#For calculating linear regression, the pattern y = x * yb
import numpy as np
import matplotlib.pyplot as plot
import pandas as pd


#Import dataset
dataset = pd.read_csv('Salary_Data.csv')
X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 1].values
                
#Split dataset
from sklearn.cross_validation import train_test_split
X_train, X_test, Y_train, Y_test = train_test_split(X, y, test_size = 1/3, random_state = 0)



#Data scaling in Python
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_Transform(X_train)
X_test = sc_X.transform(X_test)

# Linear regression to tested data set
from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
regressor.fit(X_train, Y_train)

#Predict some results for new Data
y_predicted = regressor.predict(X_test)

#Displaying predicted results on plot
plot.scatter(X_train, Y_train, color = 'blue')
plot.plot(X_train, regressor.predict(X_train), color = 'red')
plot.title('Salaries with usage of training set')
plot.xlabel('Years of experience')
plot.ylabel('Salary')
plot.show()


#Displaying test results with usage of predicted values
plot.scatter(X_test, Y_test, color = 'blue')
plot.plot(X_train, regressor.predict(X_train), color = 'red')
plot.title('Salaries with usage of test set')
plot.xlabel('Years of experience')
plot.ylabel('Salary')
plot.show()

