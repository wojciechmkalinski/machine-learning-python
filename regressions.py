#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  7 00:16:44 2017

@author: wmk
"""

# Multiple linear regression

import numpy as np
import matplotlib.pyplot as plot
import pandas as pd

#Importing dataset
dataset = pd.read_csv('50_Startups.csv')
X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 4].values

                

#Data scaling (Cities)
#LabelEncoder is used to change column [Cities] as dummy variables
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelEncoder_X = LabelEncoder()
X[:, 3] = labelEncoder_X.fit_transform(X[:, 3])
onehotencoder = OneHotEncoder(categorical_features = [3])
X = onehotencoder.fit_transform(X).toarray()

#Check for dummy variable trap
X = X[:, 1:]

#Split dataset to test and train set
from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)



#Encoding dependant variable
"""labelEncoder_y = LabelEncoder()
y = labelEncoder_y.fit_transform(y)
"""

#Multiple linear Regression to the training set
from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
regressor.fit(X_train, y_train)

#Predicting test set results
y_pred = regressor.predict(X_test)



#Backward elimination 
import statsmodels.formula.api as ss
X = np.append(arr = np.ones((50, 1)).astype(int), values = X, axis = 1)
X_opt = X[:, [0,1,2,3,4,5]]
regressor_OLS = ss.OLS(endog = y, exog = X_opt).fit()
regressor_OLS.summary()
X_opt = X[:, [0,1,2,4,5]]
regressor_OLS = ss.OLS(endog = y, exog = X_opt).fit()
regressor_OLS.summary()